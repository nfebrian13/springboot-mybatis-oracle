package com.restapi.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan("com.restapi.demo")
@MapperScan("com.restapi.demo.dao")
@SpringBootApplication
public class SpringBootMybatisOracleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMybatisOracleApplication.class, args);
	}

}
