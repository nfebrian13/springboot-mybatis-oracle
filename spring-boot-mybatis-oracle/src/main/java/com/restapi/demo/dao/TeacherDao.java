package com.restapi.demo.dao;

import java.util.List;

import com.restapi.demo.domain.Teacher;

public interface TeacherDao {

	public List<Teacher> getAllTeachers();

}
