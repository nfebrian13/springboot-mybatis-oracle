package com.restapi.demo.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restapi.demo.domain.Teacher;
import com.restapi.demo.services.TeacherService;

@RestController
public class TeacherController {

	@Resource
	TeacherService teacherService;

	@RequestMapping(value = "/teachers", method = RequestMethod.GET)
	public List<Teacher> getAll() {
		System.out.println("wkwk " + teacherService.getAllTeachers().size());
		return teacherService.getAllTeachers();
	}

	@RequestMapping(value = "/testing", method = RequestMethod.GET)
	public List<Teacher> getTesting() {
		System.out.println("wkwk testing");
		System.out.println("wkwk " + teacherService.getAllTeachers().size());
		return null;
	}

}
