package com.restapi.demo.services.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.restapi.demo.dao.TeacherDao;
import com.restapi.demo.domain.Teacher;
import com.restapi.demo.services.TeacherService;

@Service("teacherService")
@Transactional(rollbackFor = Throwable.class)
public class TeacherServiceImpl implements TeacherService {

	@Resource
	private TeacherDao teacherDao;

	@Override
	@Transactional(readOnly = true)
	public List<Teacher> getAllTeachers() {
		return teacherDao.getAllTeachers();
	}
}
