package com.restapi.demo.services;

import java.util.List;

import com.restapi.demo.domain.Teacher;

public interface TeacherService {
	public List<Teacher> getAllTeachers();
}
